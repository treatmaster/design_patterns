/**
 * Created by steve on 11/24/16.
 */
public class Context {

    public static void main(String[] args) {
        House.Builder builder = new House.Builder()
                .buildFloors(1)
                .paintHouse(2)
                .paintRoof(3);
        House house = builder.build();
        house.houseInfo();
    }
}
