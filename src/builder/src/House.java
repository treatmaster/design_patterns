/**
 * Created by steve on 11/24/16.
 */
public class House {
    private int roofColor;
    private int paintingHouse;
    private int floors;

    public void setRoofColor(int roofColor) {
        this.roofColor = roofColor;
    }

    public void setPaintingHouse(int paintingHouse) {
        this.paintingHouse = paintingHouse;
    }

    public void setFloors(int floors) {
        this.floors = floors;
    }

    public void houseInfo() {
        System.out.println("Roof color: " + roofColor);
        System.out.println("Painting: " + paintingHouse);
        System.out.println("Floors: " + floors);
    }

    public static class Builder {
        private House yourHouse;

        public Builder() {
            yourHouse = new House();
        }

        public Builder paintRoof(int roof) {
            yourHouse.setRoofColor(roof);
            return this;
        }

        public Builder paintHouse(int color) {
            yourHouse.setPaintingHouse(color);
            return this;
        }

        public Builder buildFloors(int floors) {
            yourHouse.setFloors(floors);
            return this;
        }

        public House build() {
            return yourHouse;
        }
    }
}
