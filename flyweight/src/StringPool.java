import java.util.Hashtable;
import java.util.Map;

/**
 * Created by steve on 1/12/17.
 */
public class StringPool {
    private Map<Code, IString> maps;

    public StringPool() {
        maps = new Hashtable<>();

    }

    public IString getString(Code code) {
        IString value = maps.get(code);
        if (value == null) {
            value = new MyString(code.name());
            maps.put(code, value);
        }

        return value;
    }

}
