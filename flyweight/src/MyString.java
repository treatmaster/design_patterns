/**
 * Created by steve on 1/12/17.
 */
public class MyString implements IString {

    private String value;
    public MyString(String val) {
        this.value = val;
    }

    @Override
    public void print() {
        System.out.println(String.format("Your value: %s", value));
    }
}
