/**
 * Created by steve on 1/12/17.
 */
public enum Code {
    ZERO(0),
    ONE(1),
    TWO(2),
    THREE(3);

    private int code;

    Code(int code) {
        this.code = code;
    }

    @Override
    public String toString() {
        switch (code) {
            case 0:
                return "Zero";
            case 1:
                return "One";
            case 2:
                return "Two";
            case 3:
                return "Three";
            default:
                return "Zero";
        }
    }
}
