/**
 * Created by steve on 1/12/17.
 */
public class Context {
    public static void main(String[] args) {
        StringPool pool = new StringPool();

        IString one = pool.getString(Code.ONE);
        IString two = pool.getString(Code.ONE);

        IString three = new MyString(Code.ONE.name());

        one.print();
        two.print();
        three.print();
    }
}
